from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from .views import resume, home

urlpatterns = [
    path('', home, name='home'),
    path('resume/', resume, name='resume')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
