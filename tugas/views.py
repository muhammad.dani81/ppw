# Create your views here.

from django.shortcuts import render
from django.http import HttpResponse

def home(request):
	return render(request,'index.html')

def resume(request):
    return render(request,'resume.html')


